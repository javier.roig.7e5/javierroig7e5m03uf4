using System;
using System.Drawing;

namespace JavierRoig7e5M03UF4
{
    /// <summary>
    /// Aquesta és la classe pare que conté els atributs comuns que tindrán la resta de classes.
    /// </summary>
    public class FiguraGeometrica
    {
        /// <summary>
        /// Atribut de tipus enter que conté el codi de referencia de la figura.
        /// </summary>
        protected int Codi;

        /// <summary>
        /// Atribut de tipus string que conté el nom que identifica a la figura.
        /// </summary>
        protected String Nom;

        /// <summary>
        /// Atribut de tipus Color que conté el color de la figura.
        /// </summary>
        protected Color Color;

        /// <summary>
        /// Mètode constructor sense indicar cap paràmetre, actuant amb uns valors per defecte, que es decideixen
        /// amb sentit comú.
        /// </summary>
        public FiguraGeometrica()
        {
            Codi = 2;
            Nom = "Figura0";
            Color = Color.Olive;
        }

        /// <summary>
        /// Mètode constructor indicant tants paràmetres com dades membres contingui la classe.
        /// </summary>
        /// <param name="codi"> Rep un enter que conté el codi de la figura.</param>
        /// <param name="nom"> Rep un string que conté el nom de la figura.</param>
        /// <param name="color"> Rep un Color que conté el color de la figura.</param>
        public FiguraGeometrica(int codi, string nom, Color color)
        {
            Codi = codi;
            Nom = nom;
            Color = color;
        }

        /// <summary>
        /// Mètode constructor indicant com a paràmetre un objecte de la mateixa classe, el qual cal copiar.
        /// </summary>
        /*public FiguraGeometrica()
        {
            Codi = GetCodi();
            Nom = GetNom();
            Color = GetColor();
        }*/

        /// <summary>
        /// Mètode que obté el codi de la figura.
        /// </summary>
        /// <returns> Retorna un enter amb el codi de la figura.</returns>
        public int GetCodi()
        {
            return Codi;
        }

        /// <summary>
        /// Mètode que assigna un nou valor a l'atribut codi.
        /// </summary>
        /// <param name="codi"> Rep un enter que conté el codi.</param>
        public void SetCodi(int codi)
        {
            Codi = codi;
        }


        /// <summary>
        /// Mètode que obté el nom de la figura.
        /// </summary>
        /// <returns> Retorna un string amb el nom de la figura.</returns>
        public string GetNom()
        {
            return Nom;
        }

        /// <summary>
        /// Mètode que assigna un nou valor a l'atribut nom.
        /// </summary>
        /// <param name="nom"> Rep un string que conté el nom.</param>
        public void SetNom(string nom)
        {
            Nom = nom;
        }


        /// <summary>
        /// Mètode que obté el color de la figura.
        /// </summary>
        /// <returns> Retorna un Color amb el color de la figura.</returns>
        public Color GetColor()
        {
            return Color;
        }

        /// <summary>
        /// Mètode que assigna un nou valor a l'atribut color.
        /// </summary>
        /// <param name="color"> Rep un Color que conté el color.</param>
        public void SetColor(Color color)
        {
            Color = color;
        }

        /// <summary>
        /// Mètode que mostrar la informació de l’objecte actiu per consola.
        /// </summary>
        /// <returns> Retorna un string amb el que ha de mostrar per pantalla.</returns>
        public override string ToString()
        {
            string figura = "Codi: " + Codi + "\nNom: " + Nom + "\nColor: " + Color;
            return figura;
        }
    }
}
