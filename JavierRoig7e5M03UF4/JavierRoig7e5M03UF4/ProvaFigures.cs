﻿using System;
using System.Drawing;

namespace JavierRoig7e5M03UF4
{
    ///<summary>
    /// <remarks>
    ///Author: Javier Roig
    ///Date: 07/03/2023
    ///Description: Implementació les classes FiguraGeometrica, Rectangle, Cercle i Triangle.
    /// </remarks>
    /// </summary>
    /// <summary>
    /// Aquesta classe conté el menu principal que permet desplaçar-se pel programa.
    /// </summary>
    public class ProvaFigures
    {
        /// <summary>
        /// Mètode que permet iniciar l'execució del programa.
        /// </summary>
        public static void Main()
        {
            ProvaFigures programa = new ProvaFigures();
            programa.Inici();
        }

        /// <summary>
        /// Mètode que crida a les primeres funcions necesaries per que el programam comenci a treballar.
        /// </summary>
        public void Inici()
        {
            bool fi;
            do
            {
                MostrarMenu();
                fi = TractarOpcio();
            } while (!fi);
        }

        /// <summary>
        /// Mètode que mostre a l'usuari els diferents programes que pot executar.
        /// </summary>
        public void MostrarMenu()
        {
            Console.WriteLine("\nProva Figures");
            Console.WriteLine("1. Rectangle");
            Console.WriteLine("2. Cercle");
            Console.WriteLine("3. Triangle");
            Console.WriteLine("4. Sortir.");
            Console.WriteLine("Introdueix una opció: ");
        }

        /// <summary>
        /// Mètode que crida al mètode necesari per iniciar un exercici segons la resposta rebuda de l'usuari.
        /// </summary>
        public bool TractarOpcio()
        {
            int opcio = Convert.ToInt32(Console.ReadLine());
            switch (opcio)
            {
                case 1:
                    Rectangle();
                    break;
                case 2:
                    Cercle();
                    break;
                case 3:
                    Triangle();
                    break;
                case 4:
                    return true;
                default:
                    Console.WriteLine("Error. Opció Incorrecte.");
                    break;
            }

            return false;
        }

        /// <summary>
        /// Mètode que comproba els diferents mètodes de la classe Rectangle.
        /// </summary>
        public static void Rectangle()
        {
            Rectangle rectangle1 = new Rectangle();

            Console.WriteLine("Info del rectangle original: ");
            Console.WriteLine(rectangle1.ToString());

            rectangle1.SetCodi(6);
            rectangle1.SetNom("Rectangle7B");
            rectangle1.SetColor(Color.Cornsilk);
            rectangle1.SetBase(9.1);
            rectangle1.SetAltura(5.8);

            Console.WriteLine("Info del rectangle modificat: ");
            Console.WriteLine(rectangle1.ToString());

            Console.WriteLine("Perímetre: " + rectangle1.Perimetre());
            Console.WriteLine("Àrea: " + rectangle1.Area());
        }

        /// <summary>
        /// Mètode que comproba els diferents mètodes de la classe Cercle.
        /// </summary>
        public static void Cercle()
        {
            Cercle cercle1 = new Cercle();

            Console.WriteLine("Info del cercle original: ");
            Console.WriteLine(cercle1.ToString());

            cercle1.SetCodi(1);
            cercle1.SetNom("Cercle3B");
            cercle1.SetColor(Color.Fuchsia);
            cercle1.SetRadi(5.6);

            Console.WriteLine("Info del cercle modificat: ");
            Console.WriteLine(cercle1.ToString());

            Console.WriteLine("Perímetre: " + cercle1.Perimetre());
            Console.WriteLine("Àrea: " + cercle1.Area());
        }

        /// <summary>
        /// Mètode que comproba els diferents mètodes de la classe Triangle.
        /// </summary>
        public static void Triangle()
        {
            Triangle triangle1 = new Triangle();

            Console.WriteLine("Info del triangle original: ");
            Console.WriteLine(triangle1.ToString());

            triangle1.SetCodi(9);
            triangle1.SetNom("Triangle4B");
            triangle1.SetColor(Color.Aqua);
            triangle1.SetBase(8.9);
            triangle1.SetAltura(14.5);

            Console.WriteLine("Info del triangle modificat: ");
            Console.WriteLine(triangle1.ToString());

            Console.WriteLine("Àrea: " + triangle1.Area());
        }
    }
}
