using System;
using System.Drawing;

namespace JavierRoig7e5M03UF4
{
    /// <summary>
    /// Aquesta classe conté tots aquells mètodes i atributs de l'objecte Cercle.
    /// </summary>
    public class Cercle
    {
        /// <summary>
        /// Atribut de tipus enter que conté el codi de referencia de la figura.
        /// </summary>
        protected int Codi;

        /// <summary>
        /// Atribut de tipus string que conté el nom que identifica a la figura.
        /// </summary>
        protected String Nom;

        /// <summary>
        /// Atribut de tipus Color que conté el color de la figura.
        /// </summary>
        protected Color Color;

        private double _radi;
        
        /// <summary>
        /// Mètode constructor sense indicar cap paràmetre, actuant amb uns valors per defecte, que es decideixen
        /// amb sentit comú.
        /// </summary>
        public Cercle()
        {
            Codi = 3;
            Nom = "Cercle3";
            Color = Color.Chartreuse;
            _radi = 15.6;
        }
        
        /// <summary>
        /// Mètode constructor indicant tants paràmetres com dades membres contingui la classe.
        /// </summary>
        /// <param name="codi"> Rep un enter que conté el codi de la figura.</param>
        /// <param name="nom"> Rep un string que conté el nom de la figura.</param>
        /// <param name="color"> Rep un Color que conté el color de la figura.</param>
        /// <param name="radi"></param>
        public Cercle(int codi, string nom, Color color, double radi)
        {
            Codi = codi;
            Nom = nom;
            Color = color;
            _radi = radi;
        }
        
        /// <summary>
        /// Mètode constructor indicant com a paràmetre un objecte de la mateixa classe, el qual cal copiar.
        /// </summary>
        /*public Cercle()
        {
            Codi = GetCodi();
            Nom = GetNom();
            Color = GetColor();
            _radi = GetRadi();
        }*/

        /// <summary>
        /// Mètode que obté el codi de la figura.
        /// </summary>
        /// <returns> Retorna un enter amb el codi de la figura.</returns>
        public int GetCodi()
        {
            return Codi;
        }

        /// <summary>
        /// Mètode que assigna un nou valor a l'atribut codi.
        /// </summary>
        /// <param name="codi"> Rep un enter que conté el codi.</param>
        public void SetCodi(int codi)
        {
            Codi = codi;
        }


        /// <summary>
        /// Mètode que obté el nom de la figura.
        /// </summary>
        /// <returns> Retorna un string amb el nom de la figura.</returns>
        public string GetNom()
        {
            return Nom;
        }

        /// <summary>
        /// Mètode que assigna un nou valor a l'atribut nom.
        /// </summary>
        /// <param name="nom"> Rep un string que conté el nom.</param>
        public void SetNom(string nom)
        {
            Nom = nom;
        }


        /// <summary>
        /// Mètode que obté el color de la figura.
        /// </summary>
        /// <returns> Retorna un Color amb el color de la figura.</returns>
        public Color GetColor()
        {
            return Color;
        }

        /// <summary>
        /// Mètode que assigna un nou valor a l'atribut color.
        /// </summary>
        /// <param name="color"> Rep un Color que conté el color.</param>
        public void SetColor(Color color)
        {
            Color = color;
        }


        /// <summary>
        /// Mètode que obté el radi de la figura.
        /// </summary>
        /// <returns> Retorna un double amb el radi de la figura.</returns>
        public double GetRadi()
        {
            return _radi;
        }


        /// <summary>
        /// Mètode que assigna un nou valor a l'atribut radi.
        /// </summary>
        /// <param name="radi"> Rep un double que conté el radi.</param>
        public void SetRadi(double radi)
        {
            _radi = radi;
        }


        /// <summary>
        /// Mètode que mostrar la informació de l’objecte actiu per consola.
        /// </summary>
        /// <returns> Retorna un string amb el que ha de mostrar per pantalla.</returns>
        public override string ToString()
        {
            string cercle = "Codi: " + Codi + "\nNom: " + Nom + "\nColor: " + Color + "\nRadi: " + _radi;
            return cercle;
        }


        /// <summary>
        /// Mètode que calcula el perimetre de la figura.
        /// </summary>
        /// <returns> Retorna un double com a resultat de la operació del càlcul del perímetre.</returns>
        public double Perimetre()
        {
            return 2 * Math.PI * _radi;
        }


        /// <summary>
        /// Mètode que calcula l'àrea de la figura.
        /// </summary>
        /// <returns> Retorna un double com a resultat de la operació del càlcul de l'àrea.</returns>
        public double Area()
        {
            return Math.PI * Math.Pow(_radi, 2);
        }
    }
}
