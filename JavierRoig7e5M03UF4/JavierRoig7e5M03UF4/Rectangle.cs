using System;
using System.Drawing;

namespace JavierRoig7e5M03UF4
{
    /// <summary>
    /// Aquesta classe conté tots aquells mètodes i atributs de l'objecte Rectangle.
    /// </summary>
    public class Rectangle
    {
        /// <summary>
        /// Atribut de tipus enter que conté el codi de referencia de la figura.
        /// </summary>
        protected int Codi;

        /// <summary>
        /// Atribut de tipus string que conté el nom que identifica a la figura.
        /// </summary>
        protected String Nom;

        /// <summary>
        /// Atribut de tipus Color que conté el color de la figura.
        /// </summary>
        protected Color Color;

        private double _base;
        private double _altura;

        /// <summary>
        /// Mètode constructor sense indicar cap paràmetre, actuant amb uns valors per defecte, que es decideixen
        /// amb sentit comú.
        /// </summary>
        public Rectangle()
        {
            Codi = 7;
            Nom = "Rectangle7";
            Color = Color.RoyalBlue;
            _base = 12.3;
            _altura = 7.6;
        }

        /// <summary>
        /// Mètode constructor indicant tants paràmetres com dades membres contingui la classe.
        /// </summary>
        /// <param name="codi"> Rep un enter que conté el codi de la figura.</param>
        /// <param name="nom"> Rep un string que conté el nom de la figura.</param>
        /// <param name="color"> Rep un Color que conté el color de la figura.</param>
        /// <param name="base"></param>
        /// <param name="altura"></param>
        public Rectangle(int codi, string nom, Color color, double @base, double altura)
        {
            Codi = codi;
            Nom = nom;
            Color = color;
            _base = @base;
            _altura = altura;
        }

        /// <summary>
        /// Mètode constructor indicant com a paràmetre un objecte de la mateixa classe, el qual cal copiar.
        /// </summary>
        /*public Rectangle()
        {
            Codi = GetCodi();
            Nom = GetNom();
            Color = GetColor();
            _base = GetBase();
            _altura = GetAltura();
        }*/

        /// <summary>
        /// Mètode que obté el codi de la figura.
        /// </summary>
        /// <returns> Retorna un enter amb el codi de la figura.</returns>
        public int GetCodi()
        {
            return Codi;
        }

        /// <summary>
        /// Mètode que assigna un nou valor a l'atribut codi.
        /// </summary>
        /// <param name="codi"> Rep un enter que conté el codi.</param>
        public void SetCodi(int codi)
        {
            Codi = codi;
        }


        /// <summary>
        /// Mètode que obté el nom de la figura.
        /// </summary>
        /// <returns> Retorna un string amb el nom de la figura.</returns>
        public string GetNom()
        {
            return Nom;
        }

        /// <summary>
        /// Mètode que assigna un nou valor a l'atribut nom.
        /// </summary>
        /// <param name="nom"> Rep un string que conté el nom.</param>
        public void SetNom(string nom)
        {
            Nom = nom;
        }


        /// <summary>
        /// Mètode que obté el color de la figura.
        /// </summary>
        /// <returns> Retorna un Color amb el color de la figura.</returns>
        public Color GetColor()
        {
            return Color;
        }


        /// <summary>
        /// Mètode que assigna un nou valor a l'atribut color.
        /// </summary>
        /// <param name="color"> Rep un Color que conté el color.</param>
        public void SetColor(Color color)
        {
            Color = color;
        }

        /// <summary>
        /// Mètode que obté la base de la figura.
        /// </summary>
        /// <returns> Retorna un double amb la base de la figura.</returns>
        public double GetBase()
        {
            return _base;
        }

        /// <summary>
        /// Mètode que assigna un nou valor a l'atribut base.
        /// </summary>
        /// <param name="base"> Rep un double que conté la base.</param>
        public void SetBase(double @base)
        {
            _base = @base;
        }


        /// <summary>
        /// Mètode que obté l'altura de la figura.
        /// </summary>
        /// <returns> Retorna un double amb l'altura de la figura.</returns>
        public double GetAltura()
        {
            return _altura;
        }

        /// <summary>
        /// Mètode que assigna un nou valor a l'atribut altura.
        /// </summary>
        /// <param name="altura"> Rep un double que conté l'altura.</param>
        public void SetAltura(double altura)
        {
            _altura = altura;
        }


        /// <summary>
        /// Mètode que mostrar la informació de l’objecte actiu per consola.
        /// </summary>
        /// <returns> Retorna un string amb el que ha de mostrar per pantalla.</returns>
        public override string ToString()
        {
            string rectangle = "Codi: " + Codi + "\nNom: " + Nom + "\nColor: " + Color + "\nBase: " + _base +
                               "\nAltura: " + _altura;
            return rectangle;
        }


        /// <summary>
        /// Mètode que calcula el perimetre de la figura.
        /// </summary>
        /// <returns> Retorna un double com a resultat de la operació del càlcul del perímetre.</returns>
        public double Perimetre()
        {
            return (_base * 2) + (_altura * 2);
        }


        /// <summary>
        /// Mètode que calcula l'àrea de la figura.
        /// </summary>
        /// <returns> Retorna un double com a resultat de la operació del càlcul de l'àrea.</returns>
        public double Area()
        {
            return _base * _altura;
        }
    }
}
